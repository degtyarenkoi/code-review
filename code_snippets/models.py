import uuid

from django.conf import settings
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save
from django.utils import timezone
from accounts.models import CustomUser, UserPlace, USER_ROLES, DOCTOR_ROLE, Following, UserSpecialty, NPIData, TaxonomySpecialization
from math import radians, cos, sin, asin, sqrt
from datetime import datetime
from utils.email_service import EmailService
from utils.enum import enum
from utils.parse_urls import build_url_by_list, make_base_url, clear_field_text, parse_city_and_state, parse_base_url
import pytz
import random
import string

from services.s3_service import S3Service

IN_OFFICE_LOCATION_DISTANCE = 150
DATE_TIME_FORMAT = '%Y-%m-%dT%H:%M:%S.%fZ'
CREATED_DATE_DAY_FORMAT = '%B %d'
CREATED_DATE_TIME_FORMAT = '%-I:%M %p'

COMMENT_TYPES = enum(INITIAL=0, REGULAR=1)
COMMENT_TYPES.CHOICES = (
    (COMMENT_TYPES.INITIAL, u'owner\'s comment'),
    (COMMENT_TYPES.REGULAR, u'regular comment')
)

COUNT_LIKES = 10


class Photo(models.Model):
    owner = models.ForeignKey(CustomUser, related_name="owners")
    user = models.ForeignKey(CustomUser, related_name="users", null=True, blank=True, on_delete=models.SET_NULL)
    hidden_for_user = models.BooleanField(default=False)
    url = models.URLField(max_length=1024, default='')
    thumbnail_url = models.URLField(max_length=1024, default='')
    created = models.DateTimeField(default=timezone.now(), blank=False)
    published = models.BooleanField(default=False)
    shared = models.BooleanField(default=False) #TODO check if we use it
    place = models.ForeignKey(UserPlace, related_name="places", null=True, blank=True, on_delete=models.SET_NULL)
    in_office_location = models.BooleanField(default=False)
    photo_options = models.ForeignKey('PhotoOptions', null=True, blank=True)
    report = models.TextField(max_length=1024, null=True, blank=True)
    count_likes = models.IntegerField(default=0, null=True, blank=True)
    users_likes = models.ManyToManyField(CustomUser, related_name="users_likes", null=True, blank=True)
    hide_for_users = models.ManyToManyField(CustomUser, related_name="hide_for_users", null=True, blank=True)
    photo_unique_key = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    is_private = models.BooleanField(default=False)
    npi_place = models.ForeignKey(NPIData, null=True, blank=True)

    class Meta:
        db_table = "Photo"

    def __str__(self):
        return str(self.id) +  ', Owner: ' + str(self.owner.email) + ' | User: ' + str(self.user)

    @staticmethod
    def get_photos_dict_by_place_id(place_id):
        photos = Photo.objects.filter(place_id=place_id, published=True, hidden_for_user=False).order_by('-created')
        return [photo.to_dict() for photo in photos]

    @staticmethod
    def get_photos_for_npi(npi):
        photos = Photo.objects.filter(npi_place=npi, published=True, hidden_for_user=False).order_by('-created')
        return [photo.to_dict() for photo in photos]

    @staticmethod
    def photos_to_dict(photos):
        return [photo.to_dict() for photo in photos]

    def get_users_likes(self):
        return [user.to_short_like_dict() for user in self.users_likes.all()]

    def get_hide_for_users(self):
        return [user.id for user in self.hide_for_users.all()]

    def like_photo(self, user):
        self.users_likes.add(user)
        try:
            self.count_likes += 1
        except TypeError:
            self.count_likes = 0
        self.save()

    def unlike_photo(self, user):
        self.users_likes.remove(user)
        try:
            self.count_likes -= 1
        except TypeError:
            self.count_likes = 0
        self.save()

    def make_photo_private(self):
        self.is_private = True
        self.save()

    def make_photo_public(self):
        self.is_private = False
        self.save()

    @staticmethod
    def get_photo_by_id(id):
        return Photo.objects.filter(id=id).first()

    @staticmethod
    def get_photo_by_unique_key(unique_key):
        return Photo.objects.filter(photo_unique_key=unique_key).first()

    @staticmethod
    def get_photos_by_place_id(place_id):
        return Photo.objects.filter(place__place_id=place_id, published=True, hidden_for_user=False)

    @staticmethod
    def get_photos_to_connect_with(place_id, request_user):
        if request_user.staff:
            return Photo.objects.filter(place__place_id=place_id, user_id=None, owner__role=USER_ROLES.PATIENT, hidden_for_user=False).exclude(owner_id=request_user.id)
        else:
            return Photo.objects.filter(Q(user__staff=True) | Q(user_id=None), owner__role=USER_ROLES.PATIENT, place__place_id=place_id, hidden_for_user=False).exclude(owner_id=request_user.id)

    def assign_photo_to_user(self, user):
        self.user = user
        self.save()

    @staticmethod
    def assign_photos_to_user(photos, user):
        for photo in photos:
            photo.apply_user_for_photo(user)

    def apply_user_for_photo(self, user):
        if self.owner != user and not self.user:
            self.user = user
            self.save()

    def check_place_is_right_office(self, place):
        office_location = None
        if not self.photo_options.lng and not self.photo_options.lat:
            return office_location

        distance = get_distance_between_locations(float(self.photo_options.lng),
                                                  float(self.photo_options.lat),
                                                  float(place.lng),
                                                  float(place.lat))
        if distance < IN_OFFICE_LOCATION_DISTANCE:
            office_location = place
        return office_location

    @staticmethod
    def get_user_photos(user, from_photo=0, to_photo=50):
        base_query = Photo.objects.filter(
            Q(owner=user, published=True) |
            Q(user=user, published=True, hidden_for_user=False)
        ).order_by('-created')
        user_photos = Photo.append_secret_key_to_patient_photos(base_query[from_photo:to_photo])
        count = base_query.count()
        return user_photos, count

    @staticmethod
    def get_user_photos_without_private(user, from_photo=0, to_photo=50):
        base_query = Photo.objects.filter(
            Q(owner=user, published=True) |
            Q(user=user, published=True, hidden_for_user=False),
            is_private=False
        ).order_by('-created')
        user_photos = base_query[from_photo:to_photo]
        count = base_query.count()
        return [photo.to_dict() for photo in user_photos], count

    @staticmethod
    def get_user_and_followers_photos(user, from_photo=0, to_photo=50):
        followers_id_list = Following.get_user_followers_id_list(user)
        followers_id_list_with_out_user = followers_id_list[:]
        followers_id_list.append(user)
        base_query = Photo.objects.filter(
            Q(owner_id__in=followers_id_list) |
            Q(user_id__in=followers_id_list, hidden_for_user=False), published=True
        ).distinct().order_by('-created').exclude(Q(user_id=user, hidden_for_user=True))
        base_query = Photo.exclude_followers_private_photos(followers_id_list_with_out_user, base_query)
        user_photos = Photo.append_secret_key_to_patient_photos(base_query[from_photo:to_photo])
        count = base_query.count()
        return user_photos, count

    @staticmethod
    def get_staff_and_doctor_and_followers_photos(user):
        user_object = CustomUser.get_user_object_by_id(user)
        user_photos, count = Photo.get_user_and_followers_photos(user)
        if user_object.owner:
            doctors_photos, doctors_photos_count = Photo.get_user_photos(user_object.owner)
            for doctors_photo in doctors_photos:
                user_photos.append(doctors_photo)
            count = count + doctors_photos_count
            user_photos = sorted(user_photos, key=lambda d: d['created'], reverse=True)
        return user_photos, count

    @staticmethod
    def exclude_followers_private_photos(followers_id_list, base_query):
        return base_query.exclude(Q(owner_id__in=followers_id_list) |
            Q(user_id__in=followers_id_list, hidden_for_user=False), is_private=True)

    @staticmethod
    def get_all_photo_for_doctor_and_followers(user_id, from_photo=0, to_photo=50):
        user = CustomUser.objects.get(id=user_id)
        photos, count = Photo.get_doctor_and_followers_photos(user)
        return photos[from_photo:to_photo], count

    @staticmethod
    def get_photos_for_doctor_with_out_private(user_id, from_photo=0, to_photo=50):
        user = CustomUser.objects.get(id=user_id)
        doctors_and_staff = user.get_doctors_and_staff_list_for_user()
        result_list = []
        for personal in doctors_and_staff:
            photos, count = Photo.get_user_photos_without_private(personal)
            [result_list.append(photo) for photo in photos if photo not in result_list]

        result_list = sorted(result_list, key=lambda result: result['created'], reverse=True)
        return result_list[from_photo:to_photo], len(result_list)

    @staticmethod
    def get_latest_photos(from_photo=0, to_photo=100):
        base_query = Photo.objects.filter(published=True, is_private=False).order_by('-created')
        photos_to_dict = base_query[from_photo:to_photo]
        photos = [photo.to_dict() for photo in photos_to_dict]
        count = base_query.count()
        return photos, count

    @staticmethod
    def get_photos_by_taken_date_posted_by_doctors_and_staff(taken_date, doctors_and_staff_list_id):
        return Photo.objects.filter(Q(owner__in=doctors_and_staff_list_id) | Q(user__in=doctors_and_staff_list_id),
                                    photo_options__taken_date__date__lte=taken_date.date()).order_by('-created')

    @staticmethod
    def request_user_photos(user, from_photo, to_photo):
        if user['role'] == DOCTOR_ROLE and not user['staff']:
            user_photos, count = Photo.get_all_photo_for_doctor(user['id'],from_photo, to_photo)
        else:
            user_photos, count = Photo.get_user_photos(user['id'], from_photo, to_photo)
        return user_photos, count

    @staticmethod
    def evaluate_user_photos(user, from_photo, to_photo):
        if user['role'] == DOCTOR_ROLE and not user['staff']:
            user_photos, count = Photo.get_photos_for_doctor_with_out_private(user['id'],from_photo, to_photo)
        else:
            user_photos, count = Photo.get_user_photos_without_private(user['id'], from_photo, to_photo)
        return user_photos, count

    def update_place(self, location_id):
        place = UserPlace.get_place_by_id(location_id)
        self.place = place
        self.save()

    def check_photo_is_in_office(self, place):
        return True if self.place and place.place_id == self.place.place_id and self.check_place_is_right_office(place) else False

    def remove_place_from_photo(self):
        self.update_in_office_value(False)
        self.place = None
        self.save()

    def update_in_office_value(self, is_in_office):
        self.in_office_location = is_in_office
        self.save()

    @staticmethod
    def share_photo(photo, user, share_user):
        try:
            secret_key = SecretKey.create_photo_secret_key(photo)
            photo.user = share_user
            photo.shared = True
            photo.save()
            EmailService().send_sharing_notification(share_user.email, secret_key.secret_key, user, photo)
        except Exception as error:
            print "Error share photo: {0}".format(error.message)


    @staticmethod
    def get_user_average_rating(user_email):
        photos_rating = Photo.objects.filter(
            (Q(owner__email=user_email, owner__role=USER_ROLES.DOCTOR) | Q(user__email=user_email, user__role=USER_ROLES.DOCTOR)),
            photo_options__rating__gte=1).values_list('photo_options__rating', flat=True)
        return round(sum(photos_rating)/len(photos_rating), 0) if photos_rating else {}

    @staticmethod
    def get_place_average_rating(place_id):
        photos_rating = Photo.objects.filter(place__place_id=place_id, photo_options__rating__gte=1).values_list('photo_options__rating', flat=True)
        return round(sum(photos_rating) / len(photos_rating), 0) if photos_rating else {}

    @staticmethod
    def get_specialties_id_from_photos():
        photos = Photo.get_not_hidden_for_user_published_and_not_private_photos()
        photo_owner_specialties = list(photos.filter(owner__specialty__isnull=False).values_list('owner__specialty__id', flat=True))
        photo_user_specialties = list(photos.filter(user__specialty__isnull=False).values_list('user__specialty__id', flat=True))
        photo_npi_place_specialties = list(photos.filter(npi_place__taxonomy_specialty__isnull=False).values_list('npi_place__taxonomy_specialty__id', flat=True))
        photo_owner_specialties.extend(photo_user_specialties)
        photo_owner_specialties.extend(photo_npi_place_specialties)
        return set(photo_owner_specialties)

    @staticmethod
    def get_not_hidden_for_user_published_and_not_private_photos():
        return Photo.objects.filter(hidden_for_user=False, published=True, is_private=False)

    @staticmethod
    def get_cities_by_photos(get_places_by_cities, get_npi_places_by_cities, selected_specialty):
        photos = Photo.get_not_hidden_for_user_published_and_not_private_photos()
        if selected_specialty:
            cleaned_specialty = parse_base_url(selected_specialty)
            photos_user_place = list(photos.filter(place__user__specialty__name__iexact=cleaned_specialty).values_list('place__city', flat=True))
            photos_user_place.extend(photos.filter(npi_place__taxonomy_specialty__name__iexact=cleaned_specialty).values_list('npi_place__business_practice_address_city', flat=True))
        else:
            photos_user_place = list(photos.filter(place__place_id__in=get_places_by_cities).values_list('place__city', flat=True))
            photos_user_place.extend(photos.filter(npi_place__id__in=get_npi_places_by_cities).values_list( 'npi_place__business_practice_address_city', flat=True))
        return photos_user_place


class PhotoOptions(models.Model):
    place_id = models.CharField(max_length=128, default='')
    lat = models.CharField(max_length=128, default='')
    lng = models.CharField(max_length=128, default='')
    taken_date = models.DateTimeField(blank=True, null=True)
    rating = models.DecimalField(default=0, max_digits=10, decimal_places=5)
    thumbs = models.NullBooleanField(default=None, blank=True, null=True)

    class Meta:
        db_table = "PhotoOptions"

    @staticmethod
    def create_photo_options(data):
        taken_date = datetime.strptime(data['takenDate'], '%Y-%m-%dT%H:%M:%S.%fZ').replace(tzinfo=pytz.UTC) if data.get(
            'takenDate') else None
        photo_options = PhotoOptions.objects.create(
            lng=data.get('lng', ''),
            lat=data.get('lat', ''),
            taken_date=taken_date,
        )
        photo_options.save()
        return photo_options

    @staticmethod
    def attach_rating_to_photo(photo, rating):
        photo.photo_options.rating = rating
        photo.photo_options.save()

    @staticmethod
    def attach_thumbs_to_photo(photo, thumbs):
        photo.photo_options.thumbs = True if int(thumbs) > 0 else False
        photo.photo_options.save()


class SecretKey(models.Model):
    photo = models.ForeignKey(Photo, related_name="secret_keys")
    secret_key = models.CharField(max_length=64, default='')
    created = models.DateTimeField(default=timezone.now())

    @staticmethod
    def generate_secret_string(length=32):
        return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(length))

    @staticmethod
    def create_photo_secret_key(photo):
        secret_string = SecretKey.generate_secret_string()
        secret_key = SecretKey.objects.create(
            photo=photo,
            secret_key=secret_string
        )
        secret_key.save()
        return secret_key

    @staticmethod
    def get_photo_by_secret_key(secret_key):
        secret_key = SecretKey.objects.filter(secret_key=secret_key).first()
        if not secret_key:
            raise KeyError('Photo with key {0} not found.'. format(secret_key))
        return secret_key.photo

    @staticmethod
    def get_secret_key_by_photo_id(id):
        secret_key_object = SecretKey.objects.filter(photo_id=id).first()
        return secret_key_object.secret_key if secret_key_object else None


class Comment(models.Model):
    user = models.ForeignKey(CustomUser, related_name="user_comments")
    photo = models.ForeignKey(Photo, related_name="comments")
    comment_type = models.IntegerField(choices=COMMENT_TYPES.CHOICES, default=None, null=True, blank=True)
    created = models.DateTimeField(default=datetime.now, blank=False)
    message = models.TextField(default='')

    class Meta:
        db_table = "Comment"

    def __str__(self):
        return str(self.id) + " " +  str(self.message) + ' ' + str(self.photo)

    def to_dict(self):
        return {
            'id': self.id,
            'user': self.user.to_short_dict(),
            'created': str(self.created.strftime(DATE_TIME_FORMAT)),
            'commentType': self.comment_type,
            'message': self.message
        }

    @staticmethod
    def get_comment_by_id(id):
        return Comment.objects.filter(id=id).first()

    @staticmethod
    def create_comment(user, photo, comment_type, message):
        comment = Comment.objects.create(
            user=user,
            photo=photo,
            comment_type=comment_type,
            message=message
        )
        return comment

    def update_comment(self, message):
        self.message=message
        self.save()
        return self

    @staticmethod
    def delete_comment(id):
        Comment.objects.filter(id=id).delete()

    @staticmethod
    def get_user_comments_for_photo(photo_id, user_email):
        return Comment.objects.filter(photo__id=photo_id, user__email=user_email).order_by('created')

    @staticmethod
    def get_initial_comment(photo_id):
        return Comment.objects.filter(photo__id=photo_id, comment_type=COMMENT_TYPES.INITIAL).first()

    @staticmethod
    def get_regular_comments(photo_id):
        return Comment.objects.filter(photo__id=photo_id, comment_type=COMMENT_TYPES.REGULAR)

    @staticmethod
    def create_or_update_comment(new_comment, message, photo, user):
        message = message if message else new_comment
        if not message:
            return ''
        comment, new = Comment.objects.get_or_create(user=user, photo=photo, comment_type=1, message=message)
        if not new:
            comment.update_comment(message)
        return comment


"""
Calculate the great circle distance between two points
on the earth (specified in decimal degrees)
"""
def get_distance_between_locations(lng1, lat1, lng2, lat2):
    lng1, lat1, lng2, lat2 = map(radians, [lng1, lat1, lng2, lat2])
    dlng = lng2 - lng1
    dlat = lat2 - lat1
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlng/2)**2
    c = 2 * asin(sqrt(a))
    km = 6367 * c
    return int(km * 1000)
