from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View
from accounts.models import UserPlace
from django.http import HttpResponse, HttpResponseBadRequest
from utils.glapi_service import GooglePlaceApiService, GOOGLE_SUCCESS_STATUS, TEXT_SEARCH_URL
import json
import urllib


class GoogleApiSearchByLocationView(View):

    def get(self, request):
        try:
            latitude = request.GET.get('latitude')
            longitude = request.GET.get('longitude')
            glapi = GooglePlaceApiService()
            data = glapi.find_places_by_location(latitude, longitude)
            return HttpResponse(json.dumps(data))
        except KeyError:
            return HttpResponseBadRequest('Photo without data')

class GoogleApiSearchByPlaceIdView(View):

    def get(self, request):
        place_id = request.GET.get('placeId')
        if not place_id:
            return HttpResponseBadRequest('Google API: place ID is required.')
        glapi = GooglePlaceApiService()
        data = glapi.find_place_by_id(place_id)
        formatted_place = UserPlace.parse_google_place_into(data['result'])
        if data['status'] != GOOGLE_SUCCESS_STATUS:
            return HttpResponseBadRequest('Google API: request failed.')
        return HttpResponse(json.dumps(formatted_place))


class GoogleApiSearchByTextView(View):

    def post(self, request):
        try:
            data = json.loads(request.body)
        except ValueError:
            data = request.body
        try:
            results, url = self.find_user_places(data)
            results['url'] = url
        except (ValueError, TypeError, IndexError, KeyError) as error:
            return HttpResponseBadRequest(error)
        return HttpResponse(json.dumps(results))

    @staticmethod
    def find_user_places(data):
        results =[]
        query = {}
        glapi = GooglePlaceApiService()
        if type(data) == dict:
            location_name = data.get('name')
            formatted_address = data.get('formatted_address')
        else:
            location_name = formatted_address = data

        if location_name:
            query = {'query': location_name}
            results = glapi.find_places_by_text(query)

        if not results['results'] and formatted_address:
            query = {'query': formatted_address}
            results = glapi.find_places_by_text(query)

        url = TEXT_SEARCH_URL + urllib.urlencode(query)
        return results, url
