import uuid

from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import View
from django.http import HttpResponse, HttpResponseBadRequest
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from gallery.models import Photo, PhotoOptions, Comment, SecretKey,\
    COMMENT_TYPES, HashTag, get_distance_between_locations, IN_OFFICE_LOCATION_DISTANCE
from accounts.models import (CustomUser, UserPlace, USER_ROLES,
                             DENTAL_DOCTOR_TYPE, MEDICAL_DOCTOR_TYPE,
                             OTHER_DOCTOR_TYPE, UserSpecialty, DOCTOR_ROLE, LocationCity, LocationState, TaxonomySpecialization)
from services.s3_service import S3Service
from utils.parse_urls import parse_user_slug, parse_place_slug, parse_all_slug, parse_location, clear_field_text, make_base_url, parse_base_url, parse_city_and_state, parse_dentists_slug
from django.utils import timezone
import json


class UploadPhotoView(View):

    def __init__(self):
        self.s3_service = S3Service()

    def post(self, request):
        try:
            user_id = request.user.id
            data = request.POST.dict()
            file_obj = request.FILES.get('file')
            photo_url, thumbnail_url = self.s3_service.upload_photo(file_obj, user_id)
            user_photo = Photo().save_user_photo(user_id, photo_url, thumbnail_url, data)
            doctors_by_place = []
            photo_location = user_photo.office_location
            users = None
            if photo_location:
                user_photo.update_in_office_value(True)
                user_photo.update_place(photo_location['id'])
                if request.user.role == USER_ROLES.PATIENT:
                    users = CustomUser.get_doctor_or_staff_by_place(user_photo.place.id)

            if users:
                if users.count() == 1:
                    user_photo.user = users.first()
                else:
                    doctors_by_place = [user.to_short_dict() for user in users]

            photo_into = user_photo.to_dict()
            photo_into['doctors_by_place'] = doctors_by_place
            if 'takenDate' in data and data['takenDate'] and request.user.role == USER_ROLES.DOCTOR:
                photo_into['patients_list'] = request.user.get_patients_list(user_photo.photo_options.taken_date)
            else:
                photo_into['patients_list'] = []
            return HttpResponse(json.dumps(photo_into))
        except (ValueError, TypeError, IndexError, KeyError) as error:
            return HttpResponseBadRequest(error)


class PlacePhotosView(View):

    @csrf_exempt
    def get(self, request):
        try:
            page_slug = request.GET.get('pageSlug')
            from_photo = int(request.GET.get('fromPhoto'))
            to_photo = int(request.GET.get('toPhoto'))
            place = parse_place_slug(page_slug)
            if not page_slug:
                return HttpResponse(self.get_all_photos(from_photo, to_photo))

            place = UserPlace.get_place_by_id(place['id'])
            data = self.get_practice_photos(place, from_photo, to_photo)

            return HttpResponse(data)
        except (ValueError, TypeError, IndexError, KeyError) as error:
            return HttpResponseBadRequest(error)

    def get_practice_photos(self, place, from_photo, to_photo):
        photos = Photo.get_photos_dict_by_place_id(place.id)
        count = len(photos)
        return json.dumps([{'user_photos': list(photos)[from_photo:to_photo], 'count': count}])

    def get_all_photos(self, from_photo, to_photo):
        photos, count = Photo.get_latest_photos(int(from_photo), int(to_photo))
        return json.dumps([{'user_photos': photos, 'count': count}])


class UserPhotosView(View):

    @csrf_exempt
    def get(self, request):
        try:
            page_slug = request.GET.get('pageSlug')
            from_photo = int(request.GET.get('fromPhoto'))
            to_photo = int(request.GET.get('toPhoto'))
            user = parse_user_slug(page_slug)
            user = CustomUser.get_user_object_by_id(user['id'])

            if not page_slug:
                return HttpResponse(self.get_all_photos(from_photo, to_photo))

            if user:
                data = self.get_smile_photos(user.to_dict(), from_photo, to_photo)
            else:
                data = self.get_home_photos(from_photo, to_photo)

            return HttpResponse(data)
        except (ValueError, TypeError, IndexError, KeyError) as error:
            return HttpResponseBadRequest(error)

    def get_smile_photos(self, user, from_photo, to_photo):
        if self.request.user.id == user['id']:
            user_photos, count = Photo.request_user_photos(user, from_photo, to_photo)
        else:
            user_photos, count = Photo.evaluate_user_photos(user, from_photo, to_photo)
        return json.dumps([{'user_photos': user_photos, 'count': count}])

    def get_home_photos(self, from_photo, to_photo):
        user = CustomUser().get_user_by_id(self.request.user.id)

        if user['role'] == DOCTOR_ROLE and not user['staff']:
            user_photos, count = Photo.get_all_photo_for_doctor_and_followers(user['id'])
        elif user['role'] == DOCTOR_ROLE and user['staff']:
            user_photos, count = Photo.get_staff_and_doctor_and_followers_photos(user['id'])
        else:
            user_photos, count = Photo.get_user_and_followers_photos(user['id'])

        return json.dumps([{
            'user_photos': user_photos[from_photo:to_photo],
            'count': count,
        }])

    def get_all_photos(self, from_photo, to_photo):
        photos, count = Photo.get_latest_photos(int(from_photo), int(to_photo))
        return json.dumps([{'user_photos': photos, 'count': count}])


class EvaluatePageView(View):

    def get(self, request, *args, **kwargs):
        return render_to_response('evaluate.html')


class EvaluatePhotoView(View):

    def get(self, request):
        secret_key = request.GET.get('secretKey')
        if not secret_key:
            return HttpResponseBadRequest('Photo key is required')

        photo = SecretKey.get_photo_by_secret_key(secret_key)

        if not photo:
            return HttpResponse({'message': "Photo not found"}, status=404)

        photo_info = photo.to_dict()

        if not photo.owner.staff and photo.owner.role == USER_ROLES.DOCTOR:
            photo_info['doctor_first_practice'] = UserPlace.get_first_user_place(photo.owner.email)

        if request.user.is_authenticated():
            comment = Comment.get_user_comments_for_photo(photo.id, request.user.email).first()
            if comment:
                photo_info['comment'] = comment.to_dict()

        return HttpResponse(json.dumps(photo_info))

    @method_decorator(login_required)
    def put(self, request):
        data = json.loads(request.body)
        if not data['photo'].get('id') or not data.get('secretKey'):
            return HttpResponseBadRequest('Photo key is required')
        elif not data['photo'].get('id') and not data.get('secretKey'):
            return HttpResponse(json.dumps({'chartPage': True}))

        photo_info = data['photo']
        try:
            photo = SecretKey.get_photo_by_secret_key(data['secretKey'])
            if photo_info['approved']:
                photo_info = photo.patient_approve_photo(photo_info, request.user)
            else:
                photo_date_created = photo.created
                photo_related_doctors_and_staff_list = photo.owner.get_doctors_and_staff_list_for_user()
                photo.delete_user_photo()
                CustomUser.send_not_approved_photo_notification_email_to_doctor_and_staff(request.user, photo_date_created, photo_related_doctors_and_staff_list)
            return HttpResponse(json.dumps(photo_info))
        except Exception as error:
            return HttpResponseBadRequest(error.message)


class PhotoReportView(View):

    @method_decorator(login_required)
    def post(self, request):
        try:
            data = json.loads(request.body)
            photo_options = PhotoOptions.create_photo_options(data)
            photo_options.rating = data.get('rating', 0)
            photo_options.save()
            photo = Photo.objects.create(
                owner=request.user,
                report=data['report'],
                published=True,
                created=timezone.now(),
                photo_options=photo_options,
                photo_unique_key=uuid.uuid4(),
            )
            if data.get('selectedLocationId'):
                place = UserPlace.assign_place_to_user(data['selectedLocationId'], request.user)
                photo.update_place(place.id)

            photo.patient_share_photo_to_personal(request.user)
            return HttpResponse(json.dumps(photo.to_dict()))
        except Exception as error:
            return HttpResponseBadRequest(error)


class PhotoCommentsView(View):
    @method_decorator(login_required)
    def post(self, request):
        try:
            data = json.loads(request.body)
            photo_id = data.get('id')
            new_comment = data.get('newComment')
            if not new_comment:
                return HttpResponseBadRequest('Comment is required')
            if not photo_id:
                return HttpResponseBadRequest('Photo ID is required')

            photo = Photo.get_photo_by_id(photo_id)
            _ = Comment.create_comment(request.user, photo, COMMENT_TYPES.REGULAR, new_comment)
            photo_comments = photo.to_dict()['comments']
            return HttpResponse(json.dumps(photo_comments))
        except (TypeError, KeyError, IndexError, ValueError) as error:
            return HttpResponseBadRequest(error)

    @method_decorator(login_required)
    def put(self, request):
        try:
            data = json.loads(request.body)
            comment_id = data.get('id')
            new_comment = data.get('message')
            if not new_comment:
                return HttpResponseBadRequest('New comment cannot be empty')

            if not comment_id:
                return HttpResponseBadRequest('Comment ID is required')

            comment = Comment.get_comment_by_id(comment_id)
            if comment.user.email != request.user.email:
                return HttpResponseBadRequest('You don\'t have permission to edit this comment')

            comment.update_comment(new_comment)
            return HttpResponse('Comment successfully updated')
        except (TypeError, KeyError, IndexError, ValueError) as error:
            return HttpResponseBadRequest(error)

    @method_decorator(login_required)
    def delete(self, request):
        try:
            data = json.loads(request.body)
            comment_id = data.get('commentId')
            if not comment_id:
                return HttpResponseBadRequest('There is no comment id which allows me to delete this comment')

            comment = Comment.get_comment_by_id(comment_id)
            if comment.user.email != request.user.email:
                return HttpResponseBadRequest('You don\'t have permission to delete this comment')

            Comment.delete_comment(comment_id)
            return HttpResponse('Comment successfully removed')
        except (TypeError, KeyError, IndexError, ValueError) as error:
            return HttpResponseBadRequest(error)


class HidePhotoView(View):

    @method_decorator(login_required)
    def put(self, request):
        try:
            data = json.loads(request.body)
            request_photo = data.get('photo')
            photo_id = request_photo.get('id')

            if not photo_id:
                return HttpResponseBadRequest('Photo ID is required')

            photo = Photo.get_photo_by_id(photo_id)
            photo.hidden_for_user = request_photo.get('restorePhoto', False)
            photo.save()
            return HttpResponse('Photo successfully hidden')
        except (TypeError, KeyError, IndexError, ValueError) as error:
            return HttpResponseBadRequest(error)


class PhotoRatingView(View):

    @method_decorator(login_required)
    def delete(self, request):
        try:
            data = json.loads(request.body)
            photo_id = data.get('photoId')
            if not photo_id:
                return HttpResponseBadRequest('Photo ID is required')

            photo = Photo.get_photo_by_id(photo_id)
            if (photo.owner.email == request.user.email) or (photo.user and photo.user.email == request.user.email):
                photo.photo_options.rating = 0
            else:
                return HttpResponseBadRequest('Sorry, you don\'t have permission to remove the rating')

            photo.photo_options.save()
            return HttpResponse('Rating successfully removed')
        except (TypeError, KeyError, IndexError, ValueError) as error:
            return HttpResponseBadRequest(error)

    @method_decorator(login_required)
    def put(self, request):
        try:
            data = json.loads(request.body)
            photo_id = data.get('id')
            rating = data.get('rating')
            if not photo_id:
                return HttpResponseBadRequest('Photo ID is required')
            if not rating:
                return HttpResponseBadRequest('Rating value is required')

            photo = Photo.get_photo_by_id(photo_id)
            if (photo.owner.email == request.user.email) or (photo.user and photo.user.email == request.user.email):
                photo.photo_options.rating = rating
            else:
                return HttpResponseBadRequest('Sorry, you don\'t have permission to edit this rating')

            photo.photo_options.save()
            return HttpResponse('Rating successfully updated')
        except (TypeError, KeyError, IndexError, ValueError) as error:
            return HttpResponseBadRequest(error)


class EditReportView(View):

    @method_decorator(login_required)
    def post(self, request):
        try:
            data = json.loads(request.body)
            photo = Photo.get_photo_by_id(data['id'])
            if photo.owner == request.user:
                photo.report = data['report']
                photo.save()
                return HttpResponse()
            else:
                return HttpResponseBadRequest()
        except Exception as error:
            return HttpResponseBadRequest(error)
